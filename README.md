# mk-fxlib

This is my effect synth library, to be paired with [mk-synthlib](https://codeberg.org/madskjeldgaard/mk-synthlib). The target audience of this system is myself, and I offer no support for others to use it but feel free to try it out or steal bits of the code.

mk-fxlib is built on the ruins of [sleet](https://github.com/madskjeldgaard/Sleet), [outputfx](https://github.com/madskjeldgaard/outputfx) and other failed or abandoned systems, not to mention the awesome code shared by the community.

### Installation

Open up SuperCollider and evaluate the following line of code:
`Quarks.install("https://codeberg.org/madskjeldgaard/mk-fxlib")`
